def get_data():
    with open('input3.txt','r') as f:
        raw_data = f.read()
    return [l.split(',') for l in raw_data.split('\n')]

DX = { 'R': 1, 'L': -1, 'U': 0, 'D': 0 }
DY = { 'R': 0, 'L': 0, 'U': 1, 'D': -1 }
def build_wire(data):
    wire = {}
    last = (0,0)
    steps = 0
    for line in data:
        dir = line[0]
        for _ in range( int(line[1:]) ):
            steps += 1
            last = (last[0]+DX[dir], last[1]+DY[dir])
            if last not in wire:
                wire[last] = steps
    return wire

data = get_data()
wire1 = build_wire(data[0])
wire2 = build_wire(data[1])

crosses = [point for point in wire1 if point in wire2]
print('AoC2019-3 - Part 1:', min(abs(x) + abs(y) for (x,y) in crosses))
print('AoC2019-3 - Part 2:', min(wire1[point] + wire2[point] for point in crosses))
