def check_number(number):
    NUM_OF_DIGITS = 6
    num_string = str(number)
    if len(num_string) != NUM_OF_DIGITS:
        return False
    
    double_digit = False
    for i in range(NUM_OF_DIGITS-1):
        if num_string[i] == num_string[i+1]:
            double_digit = True
    if not double_digit:
        return False

    for i in range(NUM_OF_DIGITS-1):
        if num_string[i] > num_string[i+1]:
            return False
    return True

def get_repeating_chars(text, pos):
    count = 1
    while (pos + count) < len(text) and text[pos + count] == text[pos]:
        count += 1
    return count

def check_number2(number):
    NUM_OF_DIGITS = 6
    num_string = str(number)
    if len(num_string) != NUM_OF_DIGITS:
        return False
    
    double_digit = False
    i = 0
    while i < NUM_OF_DIGITS-1:
        digits = get_repeating_chars(num_string, i)
        if digits != 2:
            i += digits
        else:
            double_digit = True
            break
    if not double_digit:
        return False

    for i in range(NUM_OF_DIGITS-1):
        if num_string[i] > num_string[i+1]:
            return False
    return True

def get_data():
    with open('input4.txt','r') as f:
        raw_data = f.read()
    return [int(i) for i in raw_data.split('-')]

data = get_data()
count = 0
for n in range(data[0], data[1]+1):
    if check_number(n):
        count += 1

print('AoC2019-4 - Part 1:', count)

count = 0
for n in range(data[0], data[1]+1):
    if check_number2(n):
        count += 1

print('AoC2019-4 - Part 2:', count)
