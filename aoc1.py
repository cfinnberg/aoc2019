def get_data():
    with open('input1.txt','r') as f:
        data_raw = f.read()
    return [int(i) for i in data_raw.split('\n')]

def get_fuel(mass):
    fuel = (mass // 3) - 2
    if fuel > 0:
        return fuel + get_fuel(fuel)
    else:
        return 0

data = get_data()
fuel_req = sum(((mass // 3) - 2) for mass in data)
print(f'AoC2019-1 - Part 1: {fuel_req}')

fuel_req = sum(get_fuel(mass) for mass in data)
print(f'AoC2019-1 - Part 2: {fuel_req}')

