def get_data():
    with open('input5.txt','r') as f:
        raw_data = f.read()
    return [ int(i) for i in raw_data.split(',') ]

class IntCodeComputer(object):
    def __init__(self, program, input_data = []):
        self.execpos = 0
        self.program = program.copy()
        self.input_data = input_data
        self.output = []
        self.param_count = { 1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 99: 0 }

    def _get_parameters(self, paramcount):
        # Parse parameters
        param_mode = [(self.program[self.execpos] // 10**i) % 10 for i in range(2, 5)][:paramcount]
        code_params = range(self.execpos+1, self.execpos+paramcount+1)
        return [v if param_mode[i] else self.program[v] for i,v in enumerate(code_params)]

    def compute(self, input_data = [], return_on_output = True):
        self.input_data.extend(input_data)
        while True:
            inst = self.program[self.execpos] % 100
            params = self._get_parameters(self.param_count[inst])
            if inst == 1:
                # SUM
                self.program[params[2]] = self.program[params[0]] + self.program[params[1]]

            elif inst == 2:
                # MULT
                self.program[params[2]] = self.program[params[0]] * self.program[params[1]]

            elif inst == 3:
                # INPUT
                self.program[params[0]] = self.input_data.pop(0)

            elif inst == 4:
                # OUTPUT
                self.execpos += self.param_count[inst] + 1
                if return_on_output:
                    return self.program[params[0]]
                else:
                    self.output.append(self.program[params[0]])
                    continue

            elif inst == 5:
                # JUMP IF NOT ZERO
                self.execpos = self.program[params[1]] if self.program[params[0]] else self.execpos + self.param_count[inst] + 1
                continue

            elif inst == 6:
                # JUMP IF ZERO
                self.execpos = self.program[params[1]] if not self.program[params[0]] else self.execpos + self.param_count[inst] + 1
                continue

            elif inst == 7:
                # LESS THAN
                self.program[params[2]] = 1 if self.program[params[0]] < self.program[params[1]] else 0

            elif inst == 8:
                # EQUAL
                self.program[params[2]] = 1 if self.program[params[0]] == self.program[params[1]] else 0

            elif inst == 99:
                # HALT
                if return_on_output:
                    return None
                else:
                    return self.output
            else:
                print('Wrong Intcode!')
                break
            self.execpos += self.param_count[inst] + 1

data = get_data()

comp = IntCodeComputer(data)
part1 = comp.compute([1], return_on_output=False)
for i in part1[:-1]:
    assert i == 0
print(f'AoC2019-5 - Part 1: {part1[-1]}')

comp = IntCodeComputer(data)
part2 = comp.compute([5], return_on_output=False)
print(f'AoC2019-5 - Part 2: {part2[0]}')
