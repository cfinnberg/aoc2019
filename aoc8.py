def get_data():
    with open('input8.txt','r') as f:
        raw_data = f.read()
    return [ int(i) for i in raw_data ]

data = get_data()

layer_wide = 25
layer_height = 6
layer_size = layer_wide * layer_height
layers = [data[idx:idx+layer_size] for idx in range(0, len(data), layer_size)]

layer_zero = min(layers, key=lambda l: l.count(0))
print(f'AoC2019-8 - Part 1: {layer_zero.count(1) * layer_zero.count(2)}')

print(f'AoC2019-8 - Part 2:')
image = [None] * layer_size
for layer in layers:
    for i,v in enumerate(layer):
        if image[i] == None and v != 2:
            image[i] = ' █'[v]

for idx in range(0, len(image), layer_wide):
    print(''.join(image[idx:idx+layer_wide]))
