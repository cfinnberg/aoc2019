def get_data():
    with open('input6.txt','r') as f:
        raw_data = f.read()
    data = {}
    for line in raw_data.split('\n'):
        parts = line.split(')')
        if parts[0] not in data.keys():
            data[parts[0]] = { 'parent': None, 'children': [parts[1]] }
        else:
            data[parts[0]]['children'].append(parts[1])
        if parts[1] in data.keys():
            data[parts[1]]['parent'] = parts[0]
        else:
            data[parts[1]] = { 'parent': parts[0], 'children': [] }
    return data

def calc_orbit(object, level=0):
    sublevels = 0
    for subobject in orbits[object]['children']:
            sublevels += calc_orbit(subobject, level+1)
    return sublevels + level

orbits = get_data()

print(f"AoC2019-6 - Part 1: {calc_orbit('COM')}")

def calc_levellist(object):
    object_list = []
    while True:
        object = orbits[object]['parent']
        object_list.append(object)
        if object == 'COM':
            return object_list

l1 = calc_levellist('YOU')
l2 = calc_levellist('SAN')

for i in l1:
    if i in l2:
        break

print(f"AoC2019-6 - Part 2: {l1.index(i) + l2.index(i)}")
