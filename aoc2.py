def get_data():
    with open('input2.txt','r') as f:
        raw_data = f.read()
    return [ int(i) for i in raw_data.split(',')]

def compute(data):
    pos = 0
    while True:
        if data[pos] == 1:
            data[data[pos+3]] = data[data[pos+1]] + data[data[pos+2]]
            pos += 4
        elif data[pos] == 2:
            data[data[pos+3]] = data[data[pos+1]] * data[data[pos+2]]
            pos += 4
        elif data[pos] == 99:
            break
        else:
            print('Wrong Intcode!')
            break
    return data[0]

data = get_data()
temp_data = data.copy()

temp_data[1] = 12
temp_data[2] = 2
solution = compute(temp_data)

print(f'AoC2019-2 - Part 1: {solution}')

solution = None
for noun in range(100):
    for verb in range(100):
        temp_data = data.copy()
        temp_data[1] = noun
        temp_data[2] = verb
        if compute(temp_data) == 19690720:
            solution = noun * 100 + verb
            break
    if solution:
        break

if solution:
    print(f'AoC2019-2 - Part 2: {solution}')
else:
    print('No solution found!')